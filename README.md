# PiAlarm

A simple Alarm clock using a Raspberry Pi and a TM1638 Module.

## Installation

### rpi-TM1638
Requires the installation of
https://github.com/thilaire/rpi-TM1638
for usage of the TM1638 Module.

### CalDav

```
sudo pip3 install caldav
```
#### Issues?
See: https://github.com/kdmurray91/SRApy/issues/1
```
sudo apt-get install libxslt-dev
```
### iCalendar
```
sudo pip3 install icalendar
```
### VLC
```
sudo apt install vlc
sudo pip3 install python-vlc
```

Create a python file names personal.py in the git repo, with includes your access data to your personal CalDav Server
Also change the included Musicpath to the path that includes your local music.

### You want it to run on startup?
```
sudo nano /etc/rc.local
```
add the line
```
sudo python3 (pathtofolder)/start.py &
```
where (pathtofolder) is the folder, the start.py file is located in. Dont forget the '&' at the end of the line and be careful to
set all paths before, otherwise your pi will not boot anymore.

After that type
```
sudo reboot
```



## More
If you want to use the Pi At the same time as Bluetooth Receiver, follow:

https://www.raspberrypi.org/forums/viewtopic.php?t=235519

If you want to install a Spotify-Client on it follow:

https://github.com/nicokaiser/rpi-audio-receiver

Just run the setup and decline everything except the part with spotifyd

Enable Audio pass trough from microphone to Headset:

https://askubuntu.com/questions/123798/how-to-hear-my-voice-in-speakers-with-a-mic
